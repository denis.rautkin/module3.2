﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Module3_2
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");

            Task4 task4 = new Task4();
            Console.WriteLine("Input length of Fibonacci sequence: ");
            task4.TryParseNaturalNumber(Console.ReadLine().Trim(), out int source);
            int[] fibonacciSequence = task4.GetFibonacciSequence(source);

            for (int i = 0; i < fibonacciSequence.Length; i++)
            {
                Console.WriteLine("{0} elements value is: {1}", i + 1, fibonacciSequence[i]);
            }

            Task5 task5 = new Task5();
            Console.WriteLine("Input integer number for reverse: ");
            int.TryParse(Console.ReadLine(), out int result);
            int reversedNumber = task5.ReverseNumber(result);
            Console.WriteLine("Reversed number is: {0}", reversedNumber);

            Task6 task6 = new Task6();
            Console.WriteLine("Input integer array size: ");
            int.TryParse(Console.ReadLine(), out int result1);
            int[] arr = task6.GenerateArray(result1);

            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("{0} elements value is: {1}", i + 1, arr[i]);
            }

            int[] invertedArr = task6.UpdateElementToOppositeSign(arr);

            for (int i = 0; i < invertedArr.Length; i++)
            {
                Console.WriteLine("{0} elements value is: {1}", i + 1, invertedArr[i]);
            }

            Task7 task7 = new Task7();
            Console.WriteLine("Input integer array size: ");
            int.TryParse(Console.ReadLine(), out int result2);
            int[] arr2 = task7.GenerateArray(result2);

            for (int i = 0; i < arr2.Length; i++)
            {
                Console.WriteLine("{0} elements value is: {1}", i + 1, arr2[i]);
            }

            List<int> elementsGreaterThenPrevious = task7.FindElementGreaterThenPrevious(arr2);

            foreach (int element in elementsGreaterThenPrevious)
            {
                Console.WriteLine("{0} elements value is: {1}", elementsGreaterThenPrevious.IndexOf(element) + 1, element);
            }

            Task8 task8 = new Task8();
            Console.WriteLine("Input integer array size: ");
            int.TryParse(Console.ReadLine(), out int result3);
            int[,] spiralArray = task8.FillArrayInSpiral(result3);

            for (int i = 0; i < spiralArray.GetLength(0); i++)
            {
                for (int j = 0; j < spiralArray.GetLength(1); j++)
                {
                    Console.Write(String.Format("{0,3}", spiralArray[i, j]));
                }
                Console.WriteLine();
            }
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            bool parsingResult = Int32.TryParse(input.Trim(), out result);

            if (parsingResult)
            {
                Console.WriteLine("Converted '{0}' to {1}.", input, result);
                if (result >= 0)
                {
                    parsingResult = true;
                    Console.WriteLine("Inputed number is natural.");
                }
                else
                {
                    parsingResult = false;
                    Console.WriteLine("Inputed number isn't natural.");
                }
            }
            else
            {
                Console.WriteLine("Unable to convert '{0}'.", input);
            }

            return parsingResult;
        }

        public int[] GetFibonacciSequence(int n)
        {
            int firstNumber = 0;
            int secondNumber = 1;
            int temporaryNumber;
            int[] fibonacciSequence = new int[n];

            for (int i = 0; i < n; i++)
            {
                temporaryNumber = firstNumber;
                firstNumber = secondNumber;
                secondNumber += temporaryNumber;
                fibonacciSequence[i] = temporaryNumber;
            }

            return fibonacciSequence;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            bool isNumberNegative = false;
            if (sourceNumber < 0)
            {
                isNumberNegative = true;
            }

            char[] numberCharacters = Math.Abs(sourceNumber).ToString().ToCharArray();

            StringBuilder stringResult = new StringBuilder();

            if (isNumberNegative)
            {
                stringResult.Append('-');
            }

            for (int i = numberCharacters.Length - 1; i >= 0; i--)
            {
                stringResult.Append(numberCharacters[i]);
            }

            int.TryParse(stringResult.ToString(), out int result);

            return result;
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            if (size < 1)
            {
                size = 0;
            }

            int[] arr = new int[size];
            Random rnd = new Random();

            for (int i = 0; i < size; i++)
            {
                arr[i] = rnd.Next(int.MinValue, int.MaxValue);
            }

            return arr;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; i++)
            {
                source[i] *= -1;
            }

            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            Task6 task6 = new Task6();

            return task6.GenerateArray(size);
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> elementsGreaterThenPrevious = new List<int>();

            for (int i = 1; i < source.Length; i++)
            {
                if (source[i] > source[i - 1])
                {
                    elementsGreaterThenPrevious.Add(source[i]);
                }
            }

            return elementsGreaterThenPrevious;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int[,] spiralArray;

            if (size < 0)
            {
                spiralArray = new int[0, 0];
            }
            else
            {
                spiralArray = new int[size, size];
            }

            int currentSide = size;
            int moveTo = 0;
            int currentValue = 1;

            static void FillTopSide(ref int[,] spiralArray, ref int moveTo, ref int currentSide, ref int currentValue)
            {
                for(int i = 0; i < currentSide; i++)
                {
                    spiralArray[moveTo, (moveTo + i)] = currentValue;
                    currentValue++;
                }
            }

            static void FillRightSide(ref int[,] spiralArray, int size, ref int moveTo, ref int currentSide, ref int currentValue)
            {
                for (int i = 0; i < currentSide - 2; i++)
                {
                    spiralArray[(1 + moveTo + i), (size - 1 - moveTo)] = currentValue;
                    currentValue++;
                }
            }

            static void FillBottomSide(ref int[,] spiralArray, int size, ref int moveTo, ref int currentSide, ref int currentValue)
            {
                if(((size/2) - moveTo) > 0)
                {
                    for (int i = 0; i < currentSide; i++)
                    {
                        spiralArray[(size - 1 - moveTo), (size - 1 - i - moveTo)] = currentValue;
                        currentValue++;
                    }
                }
            }

            static void FillLeftSide(ref int[,] spiralArray, int size, ref int moveTo, ref int currentSide, ref int currentValue)
            {
                for (int i = 0; i < currentSide - 2; i++)
                {
                    spiralArray[(size - 2 - moveTo - i), moveTo] = currentValue;
                    currentValue++;
                }
            }

            while (moveTo < (size / 2.0))
            {
                FillTopSide(ref spiralArray, ref moveTo, ref currentSide, ref currentValue);

                FillRightSide(ref spiralArray, size, ref moveTo, ref currentSide, ref currentValue);

                FillBottomSide(ref spiralArray, size, ref moveTo, ref currentSide, ref currentValue);

                FillLeftSide(ref spiralArray, size, ref moveTo, ref currentSide, ref currentValue);

                moveTo++;

                currentSide -= 2;
            }

            return spiralArray;

        }
    }
}
